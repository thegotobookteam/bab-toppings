package com.stgconsulting.bab.toppings.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Topping model.
 */
@Data
@NoArgsConstructor
@Entity
public class Topping {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id", required = true, dataType = "Long", example = "123")
    private Long id;

    @ApiModelProperty(value = "name", required = true, dataType = "String", example = "Sliced Cheese")
    private String name;

    @ApiModelProperty(value = "description", required = true, dataType = "String", example = "American cheese is processed cheese made from a blend of milk, milk fats and solids, with other fats and whey protein concentrate.")
    private String description;

    @ApiModelProperty(value = "image_path", required = true, dataType = "String", example = "SLICED_CHESSE.jpg")
    @JsonProperty(value = "image_path")
    private String imagePath;
}
