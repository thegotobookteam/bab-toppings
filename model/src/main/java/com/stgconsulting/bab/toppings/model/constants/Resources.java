package com.stgconsulting.bab.toppings.model.constants;

import static com.stgconsulting.bab.toppings.model.constants.Headers.TOPPING_ID;

/**
 * Resource constants.
 *
 * Note:
 * By convention, such fields have names consisting of capital letters, with words separated by underscores.
 * It is critical that these fields contain either primitive values or references to immutable objects.
 */
public class Resources {

    public static final String REST_PREFIX = "/rest";
    public static final String BASE_RESOURCE = "/toppings";

    public static final String API_TOPPING_ALL_URI = BASE_RESOURCE;
    public static final String API_TOPPING_ALL_DESC = "Retrieve list of all hamburger toppings.";

    public static final String API_TOPPING_BY_ID_URI = BASE_RESOURCE + "/{" + TOPPING_ID + "}";
    public static final String API_TOPPING_BY_ID_DESC = "Retrieve details of a hamburger topping.";

    public static final String API_TOPPING_CREATE_URI = BASE_RESOURCE;
    public static final String API_TOPPING_CREATE_DESC = "Create a hamburger topping.";

    public static final String API_TOPPING_UPDATE_URI = BASE_RESOURCE;
    public static final String API_TOPPING_UPDATE_DESC = "Update a hamburger topping information.";

    public static final String API_TOPPING_DELETE_BY_ID_URI = BASE_RESOURCE + "/{" + TOPPING_ID + "}";
    public static final String API_TOPPING_DELETE_BY_ID_DESC = "Delete a hamburger topping.";

    public static final String API_TOPPING_IMAGE_BY_ID_URI = BASE_RESOURCE + "/{" + TOPPING_ID + "}/image";
    public static final String API_TOPPING_IMAGE_BY_ID_DESC = "Retrieve topping image.";

}
