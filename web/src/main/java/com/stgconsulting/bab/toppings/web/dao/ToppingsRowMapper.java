package com.stgconsulting.bab.toppings.web.dao;

import com.stgconsulting.bab.toppings.model.Topping;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Topping Row Mapper class.
 */
public class ToppingsRowMapper implements RowMapper<Topping> {

    @Override
    public Topping mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Topping topping = new Topping();

        topping.setId(resultSet.getLong("id"));
        topping.setName(resultSet.getString("name"));
        topping.setDescription(resultSet.getString("description"));
        topping.setImagePath(resultSet.getString("image_path"));

        return topping;
    }
}
