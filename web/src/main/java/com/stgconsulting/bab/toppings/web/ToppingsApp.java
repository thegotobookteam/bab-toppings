package com.stgconsulting.bab.toppings.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

/**
 * Application driver.
 * <p>
 * o Docker run:  docker run --rm -p 8004 --name bab-toppings bab-toppings-web:1.LOCAL
 * o Docker run:  docker run --rm -p 8004 --network host --name bab-toppings bab-toppings-web:1.LOCAL
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
@PropertySource( "classpath:build.properties" )
public class ToppingsApp {

    public static void main( String[] args ) {
        //log.info( "main() :: Enter");

        SpringApplication.run(ToppingsApp.class, args);

        //log.info( "main() :: Exit" );
    }

}
