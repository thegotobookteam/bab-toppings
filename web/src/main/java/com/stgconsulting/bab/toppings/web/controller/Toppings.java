package com.stgconsulting.bab.toppings.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.stgconsulting.bab.toppings.model.Topping;
import com.stgconsulting.bab.toppings.model.constants.Resources;
import com.stgconsulting.bab.toppings.web.service.ToppingService;
import com.stgconsulting.bab.toppings.web.util.RestPreconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.stgconsulting.bab.toppings.model.constants.Headers.TOPPING_ID;
import static com.stgconsulting.bab.toppings.model.constants.Resources.*;

@Slf4j
@RestController
@RequestMapping(Resources.REST_PREFIX)
public class Toppings {

    private ToppingService toppingService;
//    private ObjectMapper objectMapper;

    /**
     * DI Constructor
     */
    public Toppings(ToppingService pToppingService) {
        toppingService = pToppingService;
//        objectMapper = new ObjectMapper();
    }

    @GetMapping(value = API_TOPPING_ALL_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Topping> findAll() {
        //log.info("findAll() :: Enter/Exit");
        return toppingService.findAll();
    }

    @GetMapping(value = API_TOPPING_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    public Topping findById(@Valid @PathVariable(TOPPING_ID) Long pToppingId) {
        //log.info("findById() :: Enter/Exit");
        return RestPreconditions.checkFound(toppingService.findOne(pToppingId));
    }

    @PostMapping(value = API_TOPPING_CREATE_URI, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Integer create(@RequestBody Topping pTopping) {
        //log.info("create() :: Enter/Exit");
        Preconditions.checkNotNull(pTopping);
        return toppingService.create(pTopping);
    }

    @PutMapping(value = API_TOPPING_UPDATE_URI, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody Topping pTopping) {
        //log.info("update() :: Enter");
        Preconditions.checkNotNull(pTopping);
        RestPreconditions.checkFound(toppingService.findOne(pTopping.getId()));
        toppingService.update(pTopping);
        //log.info("update() :: Exit");
    }

    @DeleteMapping(value = API_TOPPING_DELETE_BY_ID_URI, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable(TOPPING_ID) Long pId) {
        //log.info("deleteById() :: Enter");
        toppingService.deleteById(pId);
        //log.info("deleteById() :: Exit");
    }

    @GetMapping(value = API_TOPPING_IMAGE_BY_ID_URI, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    byte[] getImage(@Valid @PathVariable(TOPPING_ID) Long pToppingId) throws IOException {
        //log.info("getImage() :: Enter");
        Topping topping = toppingService.findOne(pToppingId);
        InputStream in = getClass().getResourceAsStream("/images/" + topping.getImagePath());
        //log.info("getImage() :: Exit");
        return IOUtils.toByteArray(in);
    }

}
