package com.stgconsulting.bab.toppings.web.dao;

import com.stgconsulting.bab.toppings.model.Topping;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * Toppings DAO or Data Access Object.
 */
@Slf4j
@Repository
@NoArgsConstructor
public class ToppingsDao {

    private JdbcTemplate jdbcTemplate;
    private String imagesPath;

    @Autowired
    public void setDatasource(final DataSource pDataSource) {
        this.jdbcTemplate = new JdbcTemplate(pDataSource);
        final CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
        jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);
    }

    public int getCount() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM TOPPINGS", Integer.class);
    }

    public List<Topping> findAll() {
        return jdbcTemplate.query("SELECT * FROM TOPPINGS", new ToppingsRowMapper());
    }

    public Topping findOne(final long pToppingId) {
        final String query = "SELECT * FROM TOPPINGS WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{pToppingId}, new ToppingsRowMapper());
    }

    public Integer create(final Topping pTopping) {
        return jdbcTemplate.update("INSERT INTO MEATS VALUES (?, ?, ?, ?)",
                pTopping.getId(), pTopping.getName(), pTopping.getDescription(), pTopping.getImagePath());
    }

    public Integer update(final Topping pTopping) {
        return jdbcTemplate.update("UPDATE TOPPINGS SET name = ?, description = ?, file_image = ? WHERE ID = ?",
                pTopping.getName(), pTopping.getDescription(), pTopping.getImagePath(), pTopping.getId());
    }

    public Integer delete(final Long pId) {
        return jdbcTemplate.update("DELETE FROM TOPPINGS WHERE ID = ?", pId);
    }

}
