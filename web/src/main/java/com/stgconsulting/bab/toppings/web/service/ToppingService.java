package com.stgconsulting.bab.toppings.web.service;

import com.stgconsulting.bab.toppings.model.Topping;
import com.stgconsulting.bab.toppings.web.dao.ToppingsDao;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Topping service.
 */
@Service
public class ToppingService {

    private ToppingsDao toppingsDao;

    public ToppingService(ToppingsDao toppingsDao) {
        this.toppingsDao = toppingsDao;
    }

    public Topping findOne(Long pId) {
        return toppingsDao.findOne(pId);
    }

    public List<Topping> findAll() {
        return toppingsDao.findAll();
    }

    public Integer create(Topping pTopping) {
        return toppingsDao.create(pTopping);
    }

    public Integer update(Topping pTopping) {
        return toppingsDao.update(pTopping);
    }

    public Integer deleteById(Long pId) {
        return toppingsDao.delete(pId);
    }

}
