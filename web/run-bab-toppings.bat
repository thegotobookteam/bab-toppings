@echo off
rem -------------------------------------------------------------------
rem Execute Build-A-Burger micro-service
rem -------------------------------------------------------------------
echo Executing Build-A-Burger micro-service...
docker run --rm -p 8004 --network host --name bab-toppings bab-toppings-web:1.LOCAL
